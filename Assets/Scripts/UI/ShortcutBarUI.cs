﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShortcutBarUI : MonoBehaviour
{
    public Transform itemParent;
    ShortcutBarSlot[] barSlots;
    GroundPlacementController groundPlacementController;
    int curSeleted;
    ShortcutBarSlot selectedSlot;
    Item selectedItem;
    PlayerContorller playerContorller;

    void Start()
    {
        barSlots = itemParent.GetComponentsInChildren<ShortcutBarSlot>();
        curSeleted = 0;
        groundPlacementController = GroundPlacementController.instance;
        playerContorller = PlayerManager.instance.player.GetComponent<PlayerContorller>();

        selectedSlot = barSlots[curSeleted];
        selectedSlot.OnSelect();

    }

    void Update()
    {   
        MouseWheelSelected();
        if(Input.GetMouseButtonDown(0)){
            if (!playerContorller.uiOpened)
                selectedSlot.InteractWithItem(curSeleted);
        }
    }

    void MouseWheelSelected()
    {
        int old  = curSeleted;
        curSeleted  = (curSeleted + (int) Input.mouseScrollDelta.y) % 10;
        curSeleted = Mathf.Clamp(curSeleted, 0, 9);
        //Debug.LogWarning(curSeleted);
        barSlots[curSeleted].gameObject.GetComponentInChildren<Button>().Select(); // highlight the selceted slot

        ShortcutBarSlot oldSelectedSlot = barSlots[old];
        selectedSlot = barSlots[curSeleted];
        if(oldSelectedSlot != selectedSlot){
            oldSelectedSlot.Deselect();
            selectedSlot.OnSelect();
        }

    }


}
