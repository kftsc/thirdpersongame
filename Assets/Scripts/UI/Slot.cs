﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IDropHandler
{
    public Item item;
    public Image icon;
    Inventory inventory;
    DragDropManager dragDropManager;

    protected virtual void Start()
    {
        inventory = Inventory.instance;
        dragDropManager = DragDropManager.instance;
    }

    protected virtual void Update()
    {

    }
    
    public virtual void Add(Item newItem)
    {
        item = newItem;
        icon.sprite = newItem.icon;
        icon.enabled = true;
    }

    public virtual void ClearSlot()
    {
        item = null;
        icon.sprite = null;
        icon.enabled = false;
    }

    public virtual void InteractWithItem(int index)
    {
        if (item != null)
            Debug.Log("interact with " + item.name);
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("On Drop");
        Slot curDrag = dragDropManager.curDrag;
        if (curDrag != null && curDrag.item != null){
            if (this.item != null){
                // swap two slot
                Debug.LogWarning("swap");
                Item swap = curDrag.item;
                curDrag.Add(this.item);
                this.Add(swap);
                return;
            }
            Debug.LogWarning("move");
            // just place it
           this.Add(curDrag.item);
           curDrag.ClearSlot();
        }
        
    }
}
