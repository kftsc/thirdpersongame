﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShortcutBarSlot : Slot
{
    bool isSelected;
    Item oldItem;

    protected override void Start()
    {
        base.Start();
        isSelected = false;
        oldItem = null;
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void Add(Item newItem)
    {
        oldItem = item;
        base.Add(newItem);
        HandleSelected(oldItem, newItem);
    }

    public override void ClearSlot()
    {
        oldItem = item;
        base.ClearSlot();
        HandleSelected(oldItem, null);
    }

    public override void InteractWithItem(int index)
    {
        base.InteractWithItem(index);
        if(item != null)
            item.Use(index, "ShortcutBarSlot");
    }

    public void OnSelect()
    {
        isSelected = true;
        if (item != null){
            item.OnSelected();    // select the item in the slot
        }
    }

    public void Deselect()
    {
        isSelected = false;
        if(item != null){
            item.Deselected();    // deslect the item in the slot
        }
    }

    // handle drag, drop, and swap
    void HandleSelected(Item oldItem, Item newItem)
    {
        if (!isSelected)
            return;
        
        if (oldItem != null){
            oldItem.Deselected();
        }

        if(newItem != null){
            newItem.OnSelected();
        }
    }
}
