﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InventorySlot : Slot
{
    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void Add(Item newItem)
    {
        base.Add(newItem);
    }

    public override void ClearSlot()
    {
        base.ClearSlot();
    }

    public override void InteractWithItem(int index)
    {
        base.InteractWithItem(index);
        index = transform.GetSiblingIndex();
        Debug.LogWarning("click InventorySlot: " + index);
        if (item != null)
        {
            item.Use(index, "InventorySlot");
        }

    }
}
