﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
   public Transform itemsParent;
   public Transform shortcutParent;
   public GameObject inventoryUI;
   public InventorySlot curDrag;
   InventorySlot[] slots;
   ShortcutBarSlot[] shortcuts;
   int curNum;
   Inventory inventory;
   PlayerContorller playerContorller;

   void Start()
   {
       slots = itemsParent.GetComponentsInChildren<InventorySlot>();
       shortcuts = shortcutParent.GetComponentsInChildren<ShortcutBarSlot>();

       inventory = Inventory.instance;
       playerContorller = PlayerManager.instance.player.GetComponent<PlayerContorller>();

       inventoryUI.SetActive(false);

       InitialUI();
   }

   void Update()
   {
       if(Input.GetButtonDown("Inventory")){
           inventoryUI.SetActive(!inventoryUI.activeSelf);
           playerContorller.OnUIOpened(inventoryUI.activeSelf);
       }

       if (Input.GetKeyDown(KeyCode.Escape)){
           inventoryUI.SetActive(false);
           playerContorller.OnUIOpened(false);
       }

       inventory.OnInventoryChangedCallback += UpdateUI;
   }

   void InitialUI()
   {
       for(int i = 0; i < slots.Length; i++){
           if (i < inventory.items.Count){
               slots[i].Add(inventory.items[i]);
           }
       }
   }

   void UpdateUI(Item item, int option, string user)
   {
    //    for(int i = 0; i < slots.Length; i++){
    //        if (i < inventory.items.Count){
    //            slots[i].Add(inventory.items[i]);
    //        }else{
    //            slots[i].ClearSlot();
    //        }
    //    }
        curNum = 0;
        for (int i = 0; i < slots.Length; i++){
            if (slots[i].item != null)
                curNum += 1;
        }
        for(int i = 0; i < shortcuts.Length;i++){
            if(shortcuts[i].item != null)
            curNum += 1;
        }
        
        // find a empty slot to add new item in
        if (option == -1 && curNum + 1 == inventory.items.Count){
            for(int i = 0; i < shortcuts.Length;i++){
                if(shortcuts[i].item == null){
                    shortcuts[i].Add(item);
                    return;
                }

            }

            for (int i = 0; i < slots.Length; i++){
                if (slots[i].item == null){
                    slots[i].Add(item);
                    return;
                }
            }
        }

        if (option >= 0 && curNum - 1 == inventory.items.Count){
            if (user.Equals("ShortcutBarSlot") && shortcuts[option].item != null){
                Debug.LogWarning("clear shorcut");
                shortcuts[option].ClearSlot();
            }

            if(user.Equals("InventorySlot") && slots[option].item != null){
                Debug.LogWarning("clear slots");
                slots[option].ClearSlot();
            }
        }
   }
        

}
