﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : CharacterCombat
{
    CharacterStats[] opponentStats;
    PlayerContorller playerContorller;

    protected override void Start()
    {
        base.Start();
        playerContorller = GetComponent<PlayerContorller>();
    }

    public override void Attack()
    {
        base.Attack();
        opponentStats = FindObjectsOfType<CharacterStats>();
        foreach(CharacterStats stats in opponentStats){
            if (stats == myStats)
                continue;
            float distance = Vector3.Distance(playerContorller.interactionTransform.position, stats.GetComponent<Interactable>().interactionTransform.position);
            if (distance <= playerContorller.radius){
                stats.TakeDamage(myStats.damage.GetValue());
            }
        }
    }

}
