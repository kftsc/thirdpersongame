﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterStats))]
public class CharacterCombat : MonoBehaviour
{
    protected CharacterStats myStats;
    const float combatCoolDown = 5f;
    float lastAttackTime;
    public bool inCombat {get; private set;}

    // Start is called before the first frame update
    protected virtual void Start()
    {
        myStats = GetComponent<CharacterStats>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - lastAttackTime > combatCoolDown){
            inCombat = false;
        }
    }

    public virtual void Attack()
    {
        Debug.Log(transform.name + " attack!");
        inCombat = true;
        lastAttackTime = Time.time;
    }
}
