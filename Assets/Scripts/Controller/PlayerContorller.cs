﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(PlayerMotor))]
public class PlayerContorller : MonoBehaviour
{
    public float radius = 3f;
    public Transform interactionTransform;
    public Interactable focus;
    public bool animating;    // currently player animation exception walk, idel, and run

    Interactable[] interactables;
    PlayerMotor motor;
    Camera cam;
    public bool uiOpened {get; private set;}
    public Vector3 direction;
    

    // Start is called before the first frame update
    void Start()
    {
        motor = GetComponent<PlayerMotor>();
        cam = Camera.main;

        animating = false;
        uiOpened = false;

    }

    // Update is called once per frame
    void Update()
    {   
        // face focus
        if (focus != null){
            motor.FaceTarget(focus.interactionTransform);
        }

        if (animating || uiOpened)
            return;
        
        // handle input
        // get move input
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        direction = new Vector3(horizontal, 0, vertical).normalized;

        if (direction.magnitude >= 0.1f){
            motor.Move(direction);
            if (focus != null)
                focus.OnDefocus();
            focus = null;
        }else{
            motor.DecreaseSpeed();
        }
        
        // jump
        if (Input.GetButtonDown("Jump")){
            motor.Jump();
        }

        // press E tp interact
        if (Input.GetKeyDown(KeyCode.E)){
            focus = FindClosestInteractable();
            if (focus != null){
                focus.OnFocus();
                focus.Interact();
            }
        }
    }

    Interactable FindClosestInteractable()
    {
        interactables = GameObject.FindObjectsOfType<Interactable>();
        Interactable target = null;
        float minDistance = float.MaxValue;
        foreach(Interactable interactable in interactables){
            float distance = Vector3.Distance(transform.position, interactable.interactionTransform.position);
            if (distance < minDistance && distance < interactable.radius){          // less than minDistance and in the interact radius
                minDistance = distance;
                target = interactable;
            }
        }
        return target;
    }

    public void InteractWith(Interactable interactable)
    {
        if(Input.GetKeyDown(KeyCode.E)){
            Debug.Log("press E");
            // focus it and interact
            focus = interactable;
            focus.OnFocus();
            motor.FaceTarget(interactable.interactionTransform);
            interactable.Interact();
        }
    }

    public void OnUIOpened(bool active)
    {
        uiOpened = active;
        CameraManager.instance.brain.enabled = !active;
    }

    void OnDrawGizmosSelected() {
        if (interactionTransform == null)
            interactionTransform = transform;

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(interactionTransform.position, radius);    
    }
}
