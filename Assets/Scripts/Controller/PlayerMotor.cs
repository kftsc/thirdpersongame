﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : MonoBehaviour
{
    public CharacterController controller;
    public Transform cam;
    public float moveSpeed = 6f;
    public float acceleration = 5f;
    public float runSpeed = 10f;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;

    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;             // smooth the turning angle

    public Transform groundCheck;        // to check what the player collide with, in order to reset the celocity.y
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    Vector3 velocity;
    bool isGrounded;


    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);   // create sphere down player's feet to check if is grounded
        if (isGrounded && velocity.y < 0)
            velocity.y = -2f; // make sure the player is on ground
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

    }

    public void IncreaseSpeed(){
        if (moveSpeed < runSpeed){
            moveSpeed = moveSpeed + acceleration * Time.deltaTime;
        }else{
            moveSpeed = runSpeed;
        }
    }
    public void DecreaseSpeed(){
        moveSpeed = 0f;
    }

    public void Move(Vector3 direction)
    { 
        IncreaseSpeed();
        // facing
        float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
        float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
        transform.rotation = Quaternion.Euler(0f, angle, 0f);

        // move
        Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
        controller.Move(moveDirection * moveSpeed * Time.deltaTime);
    }

    public void AttackingMove(Vector3 direction)
    {
        float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
        float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
        transform.rotation = Quaternion.Euler(0f, angle, 0f);

        // move
        Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
        controller.Move(moveDirection * 1.5f * Time.deltaTime);
    }

    public void Jump()
    {
        //Debug.Log("jump: " + isGrounded);
        if (isGrounded){
           velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }
    }

    public void FaceTarget(Transform target)
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }
}
