﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraManager : MonoBehaviour
{
  #region Singleton
  public static CameraManager instance;

  void Awake()
  {
      if (instance != null){
          Debug.LogWarning("More than one instance of CameraManger found!");
          return;
      }
      instance = this;
  }
  #endregion

  public CinemachineBrain brain;
}
