﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragDropManager : MonoBehaviour
{
   #region Singleton
   public static DragDropManager instance;

   void Awake()
   {
       if (instance != null){
           Debug.LogWarning("Moew than one instance of DragDropManager found!");
           return;
       }
       instance = this;
   }
   #endregion

   public Slot curDrag;
}
