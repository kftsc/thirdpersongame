﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimator : MonoBehaviour
{
    const float locomationAnimationSmoothTime = 0.1f;
    protected Animator animator;
    PlayerMotor motor;
    PlayerCombat playerCombat;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        motor = GetComponent<PlayerMotor>();
        animator = GetComponentInChildren<Animator>();
        playerCombat = GetComponent<PlayerCombat>();

    }

    // Update is called once per frame
    protected virtual void Update()
    {
        float speedPercent = motor.moveSpeed / motor.runSpeed;
        animator.SetFloat("speedPercent", speedPercent, locomationAnimationSmoothTime, Time.deltaTime);
        animator.SetBool("inCombat", playerCombat.inCombat); 
    }
}
