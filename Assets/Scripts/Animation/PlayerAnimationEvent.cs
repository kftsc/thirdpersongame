﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationEvent : MonoBehaviour
{

    PlayerContorller playerContorller;
    Animator animator;

    void Start()
    {
        playerContorller = PlayerManager.instance.player.GetComponent<PlayerContorller>();
        animator = PlayerManager.instance.player.GetComponent<Animator>();
    }

    public void PickUpEvent()
    {
       Interactable interactable = playerContorller.focus;
       if (interactable != null){
           ItemPickUp item = (ItemPickUp) interactable;
           item.OnEventPickUp();
       }
       playerContorller.animating = false;
    }

    public void ComboEvent()
    {
        animator.SetTrigger("combo");
        //Debug.LogWarning("combo time");
    }
}
