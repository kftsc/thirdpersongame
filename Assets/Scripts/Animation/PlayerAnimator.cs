﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : CharacterAnimator
{
    PlayerContorller playerContorller;

    protected override void Start()
    {
        base.Start();
        playerContorller = GetComponent<PlayerContorller>();
        EquipmentManager.instance.OnEquipmentChangedCallback += OnEquipmentChange;
    }

    protected override void Update()
    {
        base.Update();


    }

    public void OnPickUp()
    {
        playerContorller.animating = true;
        animator.SetTrigger("pickingUp");
    }

    public void OnAxeAttack()
    {
        animator.SetTrigger("AxeAttack");
    }

    public void OnSwordAttack()
    {
        animator.SetTrigger("SwordAttack");
    }

    public void OnPickaxeAttack()
    {
        animator.SetTrigger("PickaxeAttack");
    }

    void OnEquipmentChange(Equipment newItem, Equipment oldItem)
    {
        if (newItem != null && newItem.equipmentSlot == EquipmentSlot.Tool){
            animator.SetLayerWeight(1,1);
        }else if (newItem == null && oldItem != null &&  oldItem.equipmentSlot == EquipmentSlot.Tool){
            animator.SetLayerWeight(1,0);
        }
    }
}
