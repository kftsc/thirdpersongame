﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragDropItem : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    public Canvas canvas;
    DragDropManager dragDropManager;
    RectTransform itemIconTransform;
    CanvasGroup canvasGroup;
    Image icon;

    void Awake()
    {
        itemIconTransform = GetComponent<Slot>().icon.rectTransform;
        canvasGroup = GetComponent<CanvasGroup>();
        
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("OnPointerDown");
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("OnBeginDrag");
        dragDropManager = DragDropManager.instance;
        Image seletedIcon = GetComponent<Slot>().icon;
        icon = Instantiate(seletedIcon, itemIconTransform);
        icon.rectTransform.anchoredPosition = itemIconTransform.anchoredPosition;

        canvasGroup.alpha = .6f;
        canvasGroup.blocksRaycasts = false;
        dragDropManager.curDrag = GetComponent<Slot>();
    }

    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("OnDrag");
        icon.rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
        Destroy(icon.gameObject);
    }
}
