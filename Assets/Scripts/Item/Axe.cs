﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tool", menuName = "Inventory/Equipment/Tool/Axe")]
public class Axe : Tool
{
    public override void Use(int index, string user)
    {
        base.Use(index, user);
        PlayerManager.instance.player.GetComponent<PlayerAnimator>().OnAxeAttack();
    }
}
