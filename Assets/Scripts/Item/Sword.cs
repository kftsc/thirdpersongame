﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tool", menuName = "Inventory/Equipment/Tool/Sword")]
public class Sword : Tool
{
    public override void Use(int index, string user)
    {
        base.Use(index, user);
        PlayerManager.instance.player.GetComponent<PlayerAnimator>().OnSwordAttack();
    }
}
