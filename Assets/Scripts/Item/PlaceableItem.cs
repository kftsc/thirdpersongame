﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New PlaceableItem", menuName = "Inventory/PlaceableItem")]
public class PlaceableItem : Item
{
   public override void Use(int index, string user)
   {
       if (PlayerManager.instance.player.GetComponent<PlayerContorller>().uiOpened){
           Debug.LogWarning("UI opened cannot use PlaceableItem: " + name);
           return;
       }
       base.Use(index, user);
       bool placed = GroundPlacementController.instance.Place();
       if (placed)
        Inventory.instance.Remove(this, index, user);
   }

   public override void OnSelected()
   {
       base.OnSelected();
       GroundPlacementController.instance.placeableObjectPrefab = prefab;
   }

   public override void Deselected()
   {
       base.Deselected();
       GroundPlacementController.instance.ResetController();
   }
}
