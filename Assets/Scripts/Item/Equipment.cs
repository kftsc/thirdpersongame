﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Equipment", menuName = "Inventory/Equipment/Armor")]
public class Equipment : Item
{
    public EquipmentSlot equipmentSlot;
    public SkinnedMeshRenderer mesh;
    public int damageModifier = 0;
    public int armorModifier = 0;

    public override void Use(int index, string user)
    {
        base.Use(index, user);
        EquipmentManager.instance.Equip(this);
        Inventory.instance.Remove(this, index, user);

    }

}

public enum EquipmentSlot {Tool, Head, Chest, Legs, Feet}
