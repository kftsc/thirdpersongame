﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject
{
    new public string name = "New Item";
    public Sprite icon = null;
    public GameObject prefab;
    public bool isDefault = false;

    public virtual void Use(int index, string user)
    {
        Debug.Log("Use: " + name);
    }

    public virtual void OnSelected()
    {
        Debug.Log("Selected:" + name);
    }

    public virtual void Deselected()
    {
        Debug.Log("Deselected: " + name);
    }
}
