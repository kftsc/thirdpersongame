﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tool", menuName = "Inventory/Equipment/Tool")]
public class Tool : Equipment
{
    public int range = 0;

    public override void Use(int index, string user)
    {
        if (PlayerManager.instance.player.GetComponent<PlayerContorller>().uiOpened){
           Debug.LogWarning("UI opened cannot use Tool: " + name);
           return;
       }
        PlayerManager.instance.player.GetComponent<PlayerCombat>().Attack();

        // trigger correspond attack animation in subclass
    }

    public override void OnSelected()
    {
        base.OnSelected();
        //PlayerManager.instance.player.GetComponent<CharacterStats>().damage.Add(damageModifier);
        EquipmentManager.instance.Equip(this);
    }

    public override void Deselected()
    {
        base.Deselected();
        //PlayerManager.instance.player.GetComponent<CharacterStats>().damage.Remove(damageModifier);
        EquipmentManager.instance.Unequip(this);
    }
}
