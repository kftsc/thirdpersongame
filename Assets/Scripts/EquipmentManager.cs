﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentManager : MonoBehaviour
{
    #region Singleton
    public static EquipmentManager instance;

    void Awake()
    {
        if (instance != null){
            Debug.LogWarning("More than one instance of EquipmentManager found!");
            return;
        }
        instance = this;
    }
    #endregion

    public delegate void OnEquipmentChanged(Equipment newItem, Equipment oldItem);
    public OnEquipmentChanged OnEquipmentChangedCallback;
    public SkinnedMeshRenderer targetMesh;
    public Transform meshParent;
    public Equipment[] defaultEquipments;
    Equipment[] currEquipment;
    SkinnedMeshRenderer[] curMeshes;

    void Start()
    {
        int numSlots = System.Enum.GetNames(typeof(EquipmentSlot)).Length;
        currEquipment = new Equipment[numSlots];
        curMeshes = new SkinnedMeshRenderer[numSlots];

        EquipDefaults();
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.U)){
            UnequipAll();
        }
    }

    public void Equip(Equipment item)
    {
        int slotIndex = (int) item.equipmentSlot;
        Equipment oldItem = currEquipment[slotIndex];
        Unequip(item); // unequip the item at item.slotindex first
        currEquipment[slotIndex] = item;

        // handle the mesh
        SkinnedMeshRenderer newMesh = Instantiate<SkinnedMeshRenderer>(item.mesh);
        curMeshes[slotIndex] = newMesh;
        if (item != null && item.equipmentSlot == EquipmentSlot.Tool) {
            newMesh.rootBone = meshParent.Find(item.name);
            RenderTrail(item, true);
        }else{
            newMesh.transform.parent = targetMesh.transform;
            newMesh.bones = targetMesh.bones;
            newMesh.rootBone = targetMesh.rootBone;
        }

        if (OnEquipmentChangedCallback != null)
            OnEquipmentChangedCallback.Invoke(item, oldItem);
        
    }

    public void Unequip(Equipment item)
    {
        if (item.isDefault){
            return;
        }

        int slotIndex = (int) item.equipmentSlot;
        if (currEquipment[slotIndex] == null)
            return;
        
        // handle mesh
        if (curMeshes[slotIndex] != null){
            Destroy(curMeshes[slotIndex].gameObject);
            RenderTrail(item, false);
        }

        currEquipment[slotIndex] = null;
        if(item.equipmentSlot != EquipmentSlot.Tool){
            Inventory.instance.Add(item);
        }

        if (OnEquipmentChangedCallback != null)
            OnEquipmentChangedCallback.Invoke(null, item);
        
    }

    public void UnequipAll()
    {
        foreach(Equipment equipment in currEquipment){
            if (equipment != null && equipment.equipmentSlot != EquipmentSlot.Tool){
                Unequip(equipment);
            }
        }
    }

    public void EquipDefaults()
    {
        foreach(Equipment equipment in defaultEquipments){
            Equip(equipment);
        }
    }

    void RenderTrail(Equipment item, bool render)
    {
        if (item.GetType() != typeof(Sword)){
            return;
        }        
        Transform trail = meshParent.Find("Trail");
        if (trail != null){
            trail.gameObject.SetActive(render);
        }
    }

}
