﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConvertToSkinnedMesh : MonoBehaviour
{
    [ContextMenu("Convert to skinned mesh renderer")]
    void Convert()
    {
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        SkinnedMeshRenderer skinnedMeshRenderer = gameObject.AddComponent<SkinnedMeshRenderer>();

        skinnedMeshRenderer.sharedMesh = meshFilter.sharedMesh;
        skinnedMeshRenderer.sharedMaterials = meshRenderer.sharedMaterials;

        //DestroyImmediate(meshRenderer);
        //DestroyImmediate(meshFilter);
        
        DestroyImmediate(this);
    }
}
