﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundPlacementController : MonoBehaviour
{
   #region Singleton
   public static GroundPlacementController instance;

   void Awake()
   {
       if (instance != null){
           Debug.LogWarning("More than one instance of GroundPlacementController found!");
           return;
       }
       instance = this;
   }
   #endregion

   public GameObject placeableObjectPrefab;
   public GridManager grid;
   public float rotateAngle = 10f;

   GameObject currentPlaceableObject;
   float mouseWheelRotation;
   PlayerMotor playerMotor;
   PlayerContorller playerContorller;
   CameraManager cameraManager;

   void Start()
   {
       playerMotor = PlayerManager.instance.player.GetComponent<PlayerMotor>();
       playerContorller = PlayerManager.instance.player.GetComponent<PlayerContorller>();
       cameraManager = CameraManager.instance;
   }

   void Update()
   {
       if (placeableObjectPrefab == null){
           return;
       }

       // handle key input
       HandlePlaceObject();
       if (currentPlaceableObject != null){
           playerMotor.FaceTarget(currentPlaceableObject.transform);
           // do something to manage the placeable object
           MoveCurrentObjectToMouse();
           RotateFromRightClick();
       }
   }

   void HandlePlaceObject()
   {
       if(currentPlaceableObject == null){
           currentPlaceableObject = Instantiate(placeableObjectPrefab);
       }
   }

   void MoveCurrentObjectToMouse()
   {
        currentPlaceableObject.transform.position = grid.GetNearestPointOnGrid(playerContorller.interactionTransform.position);
   }

   void RotateFromRightClick()
   {
       //mouseWheelRotation = Input.mouseScrollDelta.y * 2;
       if (Input.GetMouseButtonDown(1)){
        currentPlaceableObject.transform.Rotate(Vector3.up, 90f);
       }
   }

   public bool Place()
   {
       if (currentPlaceableObject == null){
           Debug.LogWarning("currentPlaceableObject is null!");
           return false;
       }

        if (currentPlaceableObject.GetComponent<PlaceObject>().isCollide){
            Debug.LogWarning("Collide with other");
            return false;
        }

            
        //currentPlaceableObject.GetComponent<BoxCollider>().enabled = true;
        currentPlaceableObject.GetComponent<PlaceObject>().placed = true;
        currentPlaceableObject = null;
        //cameraManager.brain.enabled = true;

        ResetController();
        return true;
   }

   public void ResetController()
   {
       if (currentPlaceableObject != null){
           Destroy(currentPlaceableObject);
           currentPlaceableObject = null;
       }
       placeableObjectPrefab = null;
   }

}
