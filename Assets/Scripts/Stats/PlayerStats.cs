﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : CharacterStats
{
    void Start()
    {
        EquipmentManager.instance.OnEquipmentChangedCallback += OnEquipmentChanged;
    }

    void OnEquipmentChanged(Equipment newItem, Equipment oldItem)
    {
        if (newItem != null){
            damage.Add(newItem.damageModifier);
            armor.Add(newItem.armorModifier);
        }

        if (oldItem != null){
            damage.Remove(oldItem.damageModifier);
            armor.Remove(oldItem.armorModifier);
        }
    }
}
