﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stat
{
    [SerializeField]

    private int baseValue = 0;
    private List<int> modifiers = new List<int>();


    public int GetValue()
    {
        int finalValue = baseValue;
        modifiers.ForEach(x => finalValue += x);
        return finalValue;
    }

    public void Add(int modifier)
    {
        if (modifier != 0){
            modifiers.Add(modifier);
        }
    }

    public void Remove(int modifier)
    {
        if (modifier != 0){
            modifiers.Remove(modifier);
        }
    }

    public List<int> GetModifiers()
    {
        return modifiers;
    }
}
