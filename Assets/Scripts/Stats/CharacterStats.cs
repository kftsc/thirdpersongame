﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    public int maxHealth = 100;
    public int curHealth {get; private set;}

    public Stat damage;
    public Stat armor;


    void Awake()
    {
        curHealth = maxHealth;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T)){
            TakeDamage(10);
        }
    }

    public void TakeDamage(int damage)
    {
        damage -= armor.GetValue();
        damage = Mathf.Clamp(damage, 0, int.MaxValue);    // do not want to due a negative damage

        curHealth -= damage;
        curHealth = Mathf.Clamp(curHealth, 0, maxHealth);

        Debug.Log(transform.name + " take damage: " + damage + "; curHealth = " + curHealth);

        if (curHealth == 0){
            Die();
        }
    }

    public virtual void Die()
    {
        Debug.Log(transform.name + " die");
    }
}
