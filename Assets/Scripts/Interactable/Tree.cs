﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : Interactable
{   
    public GameObject wood;   // prefab

    public override void Interact()
    {
        base.Interact();
        GameObject newWood = Instantiate(wood, transform.position, Quaternion.identity);
        float x = Random.Range(-3,3);
        float z = Random.Range(-3,3);
        newWood.GetComponent<Rigidbody>().velocity = new Vector3(x,0,z);

    }
    
}
