﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public float radius = 3f;
    public Transform interactionTransform;

    protected GameObject player;
    protected Transform target;   

    protected virtual void Start()
    {
        player = PlayerManager.instance.player;
        if (interactionTransform == null)
            interactionTransform = transform;
    }

    protected virtual void Update()
    {
        // if player in range, playrer will focus on this object
        // float distance = Vector3.Distance(interactionTransform.position, player.transform.position);
        // if (distance <= radius){
        //     player.GetComponent<PlayerContorller>().InteractWith(this);
       //}
    }

    public virtual void Interact()
    {
        Debug.Log("Player interact with: " + transform.name);
    }

    public void OnFocus()
    {
        target = player.transform;
    }

    public void OnDefocus()
    {
        target = null;
    }

    void OnDrawGizmosSelected() {
        if (interactionTransform == null)
            interactionTransform = transform;

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(interactionTransform.position, radius);   
    }


}
