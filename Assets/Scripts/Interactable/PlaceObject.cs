﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceObject : ItemPickUp
{
    public bool isCollide;
    public float groundDistance = 0.1f;
    public LayerMask groundMask;
    public Transform groundCheck;
    public bool placed;
    bool isGrounded;
    bool frozen;
    Rigidbody m_Rigidbody;

    protected override void Start()
    {
        base.Start();
        if (item.GetType() != typeof(PlaceableItem)){
            Debug.LogWarning("Please initialize a PlaceableItem to PlaceObject!");
        }
        isCollide = false;
        frozen = false;
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    protected override void Update()
    {
        base.Update();
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        if(isGrounded && !frozen && placed){
            m_Rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            ChangeLayers(8);  // change to ground layer
            frozen = true;
        }
    }
    
    public override void Interact()
    {
        if (!placed)
            return;
        
        base.Interact();
    }

    // void PickUp()
    // {
    //     // bool wasPickUp = Inventory.instance.Add(placeableItem);
    //     // if (wasPickUp){
    //     //     Destroy(gameObject);
    //     // }
    // }
    public void ChangeLayers(int layerIndex)
    {
        gameObject.layer = layerIndex;
        foreach (Transform child in GetComponentsInChildren<Transform>()){
            child.gameObject.layer = layerIndex;
        }
    }

    void OnCollisionStay(Collision other) {
        isCollide = true;
    }

    void OnCollisionExit(Collision other) {
        isCollide = false;
    }
}
