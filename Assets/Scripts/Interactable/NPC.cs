﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : Interactable
{
   public override void Interact()
   {
       base.Interact();
   }

   protected override void Update()
   {
       base.Update();

       if (target != null){
           FaceTarget();
       }
   }

   void FaceTarget()
   {
       Vector3 direction = (target.position - transform.position).normalized;
       Quaternion lookDirection = Quaternion.LookRotation(new Vector3(direction.x, 0 , direction.z));
       transform.rotation = Quaternion.Slerp(transform.rotation, lookDirection, Time.deltaTime * 5f);
   }
}
