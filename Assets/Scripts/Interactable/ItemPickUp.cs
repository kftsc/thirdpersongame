﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickUp : Interactable
{
    public Item item;
    PlayerAnimator playerAnimator;

    protected override void Start()
    {
        base.Start();
        playerAnimator = player.GetComponent<PlayerAnimator>();
        if (playerAnimator == null)
            Debug.LogWarning("no playerAnimator found!");

    }

    public override void Interact()
    {
        base.Interact();

        Pickup();
    }

    void Pickup()
    {  
        playerAnimator.OnPickUp();

        Debug.Log("pickup: " + item.name);     // pick up in PlayerAnimationEvnet
        

    }

    public void OnEventPickUp()
    {
        bool wasPickUP = Inventory.instance.Add(item);
        if (wasPickUP)
            Destroy(gameObject);
    }
}
