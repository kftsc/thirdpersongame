﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    #region Singleton
    public static Inventory instance;

    void Awake()
    {
        if (instance != null){
            Debug.LogWarning("More than one instance of Inventory found!");
            return;
        }
        instance = this;
    }
    #endregion

    public delegate void OnInventoryChanged(Item item, int option, string user);
    public OnInventoryChanged OnInventoryChangedCallback;

    public int space = 24;    // max space of inventory
    public List<Item> items = new List<Item>();

    public bool Add(Item item)
    {
        if(items.Count >= space){
            Debug.Log("Inventory Is Full!");
            return false;
        }

        items.Add(item);
        if (OnInventoryChangedCallback != null)
            OnInventoryChangedCallback.Invoke(item, -1, "");
        return true;
    }

    public void Remove(Item item, int index, string user)
    {
        items.Remove(item);
        if (OnInventoryChangedCallback != null)
            OnInventoryChangedCallback.Invoke(item, index, user);
    }
}
