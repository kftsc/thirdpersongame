﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    public int gridRow = 40;
    public int gridCol = 40;
    public float size = 1f;   // size of grid or between each point
    public float gizmoSize = 0.1f;

    public Vector3 GetNearestPointOnGrid(Vector3 position)
    {
        position -= transform.position;

        int xCount = Mathf.RoundToInt(position.x / size);
        int yCount = Mathf.RoundToInt(position.y / size);
        int zCount = Mathf.RoundToInt(position.z / size);

        Vector3 result = new Vector3(
            (float) xCount * size,
            (float) position.y,
            (float) zCount * size
        );

        result += transform.position;

        return result;
    }

    void OnDrawGizmosSeleted()
    {
        Gizmos.color = Color.red;

        for (float x = 0; x < gridCol; x += size){
            for (float z = 0; z < gridRow; z +=size){
                Vector3 point = GetNearestPointOnGrid(new Vector3(x, 0f,z));
                Gizmos.DrawSphere(point, gizmoSize);
            }
        }

    }


}
